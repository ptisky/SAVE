"EXERCICE 1"

Select SUM(NbPAv) as Nombre_de_places
FROM avion,compagnie
WHERE avion.NumCompAv = compagnie.NumComp
AND compagnie.NomComp = 'Quantas'

"EXERCICE 2"

Select MIN( SalPil) , AVG(SalPil) as Moyenne_de_salaire 
FROM pilote,compagnie
WHERE pilote.NumCompPil= compagnie.NumComp
AND compagnie.NatComp = 'Fr'

"EXERCICE 3"

Select SUM(NumAeroDepVol)
FROM aeroport,vol
WHERE aeroport.NumAero = NumAeroDepVol
AND aeroport.NomAero = 'StEx'

"EXERCICE 4"

Select MAX(SalPil) AS max_salaire_2005_a_2006
FROM pilote,compagnie
WHERE pilote.NumCompPil= compagnie.NumComp
AND pilote.DateEmbPil
BETWEEN '2005-01-01' AND '2006-01-01'
AND compagnie.NomComp = 'Air France'

"EXERCICE 5"

SELECT SUM(SalPil*0.5) AS salaire_avec_prime
FROM pilote
WHERE MONTH (pilote.DateEmbPil) = MONTH(NOW()) - 2

"EXERCICE 6"

SELECT NomPil, COUNT(NumPilVol)
FROM pilote,vol
WHERE pilote.NumPil = vol.NumPilVol
GROUP BY NomPil

"EXERCICE 7"

SELECT COUNT(NomPil), SUM(SalPil),AVG(SalPil) as Moyenne_de_salaire 
FROM pilote,compagnie
WHERE pilote.NumCompPil= compagnie.NumComp
ORDER BY Moyenne_de_salaire ASC