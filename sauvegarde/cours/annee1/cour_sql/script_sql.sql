CREATE TABLE 'division' (	
	'code_div' int(10) AUTO_INCREMENT,
	'libelle_niveau' varchar(50),
	'code_niveau' int(10),
	PRIMARY KEY ('code_div') );
		
CREATE TABLE 'eleve' (
	'code_eleve' int(10) AUTO_INCREMENT,
	'nom_eleve' varchar(50),
	'prenom_eleve' varchar(50),
	'date_naissance' date,
	'num_isee' int(10),
	'code_div' int(10),
	PRIMARY KEY ('code_eleve') );
  
  
CREATE TABLE 'evaluation' (
	'Num_eval' int(10) AUTO_INCREMENT,
	'date_eval' date,
	PRIMARY KEY ('Num_eval') );
  
  
CREATE TABLE 'NIVEAU' ( 
	'code_niveau' int(10) AUTO_INCREMENT,
	'libelle_niveau' varchar(50),
	PRIMARY KEY ('code_niveau') );
  
  
CREATE TABLE 'resultat' (
	'note' int(10),
	'code_eleve' int(10),
	'num_eval' int(10),
	PRIMARY KEY ('code_eleve','num_eval') );
	
	
	INSERT INTO 'division' ('code_div', 
							'libelle_niveau', 
							'code_niveau')
	VALUES
	(1, NULL, 0),
	(2, NULL, 0);
		
	INSERT INTO 'eleve'('code_eleve', 
						'nom_eleve', 
						'prenom_eleve', 
						'date_naissance', 
						'num_isee', 
						'code_div')
	VALUES
	(1, 5, 'morgan', 'roy', '1996-30-12', 5);
	
	INSERT INTO 'evaluation'('Num_eval',
							 'date_eval')
	VALUES
	(492, NULL);
	
	INSERT INTO 'NIVEAU'('code_niveau',
						 'libelle_niveau')
	VALUES
	(1, 'BTS1'),
	(2, 'BTS2'),
	(3, 'seconde'),
	(4, 'premiere'),
	(5, 'terminale'),
	(6, 'fhgdtd'),
	(7, 'fzouhgzio'),
	(8, 'zoueyeug'),
	(9, 'ozeighozegh'),
	(10, 'zmojzeiorhg');
	
	INSERT INTO 'resultat' ('note', 
							'code_eleve',
							'num_eval') 
	
	VALUES
	(20, 0, 0),
	(NULL, 0, 42),
	(NULL, 20, 0);
	
	ALTER TABLE division 
	MODIFY
	libille_niveau
    libelle_niveau varchar(50)
	