-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 29 Janvier 2016 à 14:49
-- Version du serveur :  5.6.16
-- Version de PHP :  5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `ppe`
--

-- --------------------------------------------------------

--
-- Structure de la table `appartenir`
--

CREATE TABLE IF NOT EXISTS `appartenir` (
  `REG_CODE` varchar(2) NOT NULL,
  `vis_NumSecu` int(11) NOT NULL,
  PRIMARY KEY (`REG_CODE`),
  UNIQUE KEY `REG_CODE` (`REG_CODE`),
  UNIQUE KEY `vis_NumSecu` (`vis_NumSecu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `diplome`
--

CREATE TABLE IF NOT EXISTS `diplome` (
  `code_dip` int(3) NOT NULL,
  `nom_dip` varchar(15) DEFAULT NULL,
  `niv_dip` int(1) DEFAULT NULL,
  PRIMARY KEY (`code_dip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `diplome`
--

INSERT INTO `diplome` (`code_dip`, `nom_dip`, `niv_dip`) VALUES
(1, 'aucun', 0),
(2, 'bac', 1),
(3, 'bts', 2),
(4, 'licence', 3),
(5, 'master', 4),
(6, 'doctorat', 5);

-- --------------------------------------------------------

--
-- Structure de la table `echantillonmedic`
--

CREATE TABLE IF NOT EXISTS `echantillonmedic` (
  `ech_num` int(11) NOT NULL,
  `ech_prixHT` float DEFAULT NULL,
  `med_depotLegal` int(11) DEFAULT NULL,
  `id_tva` int(2) NOT NULL,
  PRIMARY KEY (`ech_num`),
  KEY `med_depotLegal` (`med_depotLegal`),
  KEY `id_tva` (`id_tva`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `echantillonmedic`
--

INSERT INTO `echantillonmedic` (`ech_num`, `ech_prixHT`, `med_depotLegal`, `id_tva`) VALUES
(1, 20, 1, 1),
(2, 20, 2, 2),
(3, 20, 3, 3),
(4, 20, 4, 4);

-- --------------------------------------------------------

--
-- Structure de la table `medicament`
--

CREATE TABLE IF NOT EXISTS `medicament` (
  `med_depotLegal` int(11) NOT NULL AUTO_INCREMENT,
  `med_nomcommercial` varchar(25) DEFAULT NULL,
  `med_composition` varchar(25) DEFAULT NULL,
  `med_effets` varchar(194) DEFAULT NULL,
  `med_contreIndic` varchar(236) DEFAULT NULL,
  PRIMARY KEY (`med_depotLegal`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Contenu de la table `medicament`
--

INSERT INTO `medicament` (`med_depotLegal`, `med_nomcommercial`, `med_composition`, `med_effets`, `med_contreIndic`) VALUES
(1, 'TRIMYCINE', 'Triamcinolone (ac?tonide)', 'Ce m?dicament est un cortico?de ?? activit? forte ou tr?s forte associ? ?? un antibiotique et un antifongique, utilis? en application locale dans certaines atteintes cutan?es surinfect?es.', 'Ce m?dicament est contre-indiqu? en cas d''allergie ?? l''un des constituants, d''infections de la peau ou de parasitisme non trait?s, d''acn?. Ne pas appliquer sur une plaie, ni sous un pansement occlusif.'),
(2, 'ADIMOL', 'Amoxicilline + Acide clav', 'Ce m?dicament, plus puissant que les p?nicillines simples, est utilis? pour traiter des infections bact?riennes sp?cifiques.', 'Ce m?dicament est contre-indiqu? en cas d''allergie aux p?nicillines ou aux c?phalosporines.'),
(3, 'AMOPIL', 'Amoxicilline', 'Ce m?dicament, plus puissant que les p?nicillines simples, est utilis? pour traiter des infections bact?riennes sp?cifiques.', 'Ce m?dicament est contre-indiqu? en cas d''allergie aux p?nicillines. Il doit ?tre administr? avec prudence en cas d''allergie aux c?phalosporines.'),
(4, 'AMOXAR', 'Amoxicilline', 'Ce m?dicament, plus puissant que les p?nicillines simples, est utilis? pour traiter des infections bact?riennes sp?cifiques.', 'La prise de ce m?dicament peut rendre positifs les tests de d?pistage du dopage.'),
(5, 'AMOXI G?', 'Amoxicilline', 'Ce m?dicament, plus puissant que les p?nicillines simples, est utilis? pour traiter des infections bact?riennes sp?cifiques.', 'Ce m?dicament est contre-indiqu? en cas d''allergie aux p?nicillines. Il doit ?tre administr? avec prudence en cas d''allergie aux c?phalosporines.'),
(6, 'APATOUX Vitamine C', 'Tyrothricine + T?traca?ne', 'Ce m?dicament est utilis? pour traiter les affections de la bouche et de la gorge.', 'Ce m?dicament est contre-indiqu? en cas d''allergie ?? l''un des constituants, en cas de ph?nylc?tonurie et chez l''enfant de moins de 6 ans.'),
(7, 'BACTIGEL', 'Erythromycine', 'Ce m?dicament est utilis? en application locale pour traiter l''acn? et les infections cutan?es bact?riennes associ?es.', 'Ce m?dicament est contre-indiqu? en cas d''allergie aux antibiotiques de la famille des macrolides ou des lincosanides.'),
(8, 'BACTIVIL', 'Erythromycine', 'Ce m?dicament est utilis? pour traiter des infections bact?riennes sp?cifiques.', 'Ce m?dicament est contre-indiqu? en cas d''allergie aux macrolides (dont le chef de file est l''?rythromycine).'),
(9, 'BIVALIC', 'Dextropropoxyph?ne + Para', 'Ce m?dicament est utilis? pour traiter les douleurs d''intensit? mod?r?e ou intense.', 'Ce m?dicament est contre-indiqu? en cas d''allergie aux m?dicaments de cette famille, d''insuffisance h?patique ou d''insuffisance r?nale.'),
(10, 'CARTION', 'Acide ac?tylsalicylique (', 'Ce m?dicament est utilis? dans le traitement symptomatique de la douleur ou de la fi?vre.', 'Ce m?dicament est contre-indiqu? en cas de troubles de la coagulation (tendances aux h?morragies), d''ulc?re gastroduod?nal, maladies graves du foie.'),
(11, 'CLAZER', 'Clarithromycine', 'Ce m?dicament est utilis? pour traiter des infections bact?riennes sp?cifiques. Il est ?galement utilis? dans le traitement de l''ulc?re gastro-duod?nal, en association avec d''autres m?dicaments.', 'Ce m?dicament est contre-indiqu? en cas d''allergie aux macrolides (dont le chef de file est l''?rythromycine).'),
(12, 'DEPRAMIL', 'Clomipramine', 'Ce m?dicament est utilis? pour traiter les ?pisodes d?pressifs s?v?res, certaines douleurs rebelles, les troubles obsessionnels compulsifs et certaines ?nur?sies chez l''enfant.', 'Ce m?dicament est contre-indiqu? en cas de glaucome ou d''ad?nome de la prostate, d''infarctus r?cent, ou si vous avez re??u un traitement par IMAO durant les 2 semaines pr?c?dentes ou en cas d''allergie aux antid?presseurs imipraminiques.'),
(13, 'DIMIRTAM', 'Mirtazapine', 'Ce m?dicament est utilis? pour traiter les ?pisodes d?pressifs s?v?res.', 'La prise de ce produit est contre-indiqu?e en cas de d''allergie ?? l''un des constituants.'),
(14, 'DOLORIL', 'Acide ac?tylsalicylique (', 'Ce m?dicament est utilis? dans le traitement symptomatique de la douleur ou de la fi?vre.', 'Ce m?dicament est contre-indiqu? en cas d''allergie au parac?tamol ou aux salicylates.'),
(15, 'NORMADOR', 'Doxylamine', 'Ce m?dicament est utilis? pour traiter l''insomnie chez l''adulte.', 'Ce m?dicament est contre-indiqu? en cas de glaucome, de certains troubles urinaires (r?tention urinaire) et chez l''enfant de moins de 15 ans.'),
(16, 'EQUILAR', 'M?clozine', 'Ce m?dicament est utilis? pour traiter les vertiges et pour pr?venir le mal des transports.', 'Ce m?dicament ne doit pas ?tre utilis? en cas d''allergie au produit, en cas de glaucome ou de r?tention urinaire.'),
(17, 'EVEILLOR', 'Adrafinil', 'Ce m?dicament est utilis? pour traiter les troubles de la vigilance et certains symptomes neurologiques chez le sujet ag?.', 'Ce m?dicament est contre-indiqu? en cas d''allergie ?? l''un des constituants.'),
(18, 'INSECTIL', 'Diph?nydramine', 'Ce m?dicament est utilis? en application locale sur les piq?res d''insecte et l''urticaire.', 'Ce m?dicament est contre-indiqu? en cas d''allergie aux antihistaminiques.'),
(19, 'JOVENIL', 'Josamycine', 'Ce m?dicament est utilis? pour traiter des infections bact?riennes sp?cifiques.', 'Ce m?dicament est contre-indiqu? en cas d''allergie aux macrolides (dont le chef de file est l''?rythromycine).'),
(20, 'LIDOXYTRACINE', 'Oxyt?tracycline +Lidoca?n', 'Ce m?dicament est utilis? en injection intramusculaire pour traiter certaines infections sp?cifiques.', 'Ce m?dicament est contre-indiqu? en cas d''allergie ?? l''un des constituants. Il ne doit pas ?tre associ? aux r?tino?des.'),
(21, 'LITHORINE', 'Lithium', 'Ce m?dicament est indiqu? dans la pr?vention des psychoses maniaco-d?pressives ou pour traiter les ?tats maniaques.', 'Ce m?dicament ne doit pas ?tre utilis? si vous ?tes allergique au lithium. Avant de prendre ce traitement, signalez ?? votre m?decin traitant si vous souffrez d''insuffisance r?nale, ou si vous avez un r?gime sans sel.'),
(22, 'PARMOCODEINE', 'Cod?ine + Parac?tamol', 'Ce m?dicament est utilis? pour le traitement des douleurs lorsque des antalgiques simples ne sont pas assez efficaces.', 'Ce m?dicament est contre-indiqu? en cas d''allergie ?? l''un des constituants, chez l''enfant de moins de 15 Kg, en cas d''insuffisance h?patique ou respiratoire, d''asthme, de ph?nylc?tonurie et chez la femme qui allaite.'),
(23, 'PHYSICOR', 'Sulbutiamine', 'Ce m?dicament est utilis? pour traiter les baisses d''activit? physique ou psychique, souvent dans un contexte de d?pression.', 'Ce m?dicament est contre-indiqu? en cas d''allergie ?? l''un des constituants.'),
(24, 'PIRIZAN', 'Pyrazinamide', 'Ce m?dicament est utilis?, en association ?? d''autres antibiotiques, pour traiter la tuberculose.', 'Ce m?dicament est contre-indiqu? en cas d''allergie ?? l''un des constituants, d''insuffisance r?nale ou h?patique, d''hyperuric?mie ou de porphyrie.'),
(25, 'POMADINE', 'Bacitracine', 'Ce m?dicament est utilis? pour traiter les infections oculaires de la surface de l''oeil.', 'Ce m?dicament est contre-indiqu? en cas d''allergie aux antibiotiques appliqu?s localement.'),
(26, 'TROXADET', 'Parox?tine', 'Ce m?dicament est utilis? pour traiter la d?pression et les troubles obsessionnels compulsifs. Il peut ?galement ?tre utilis? en pr?vention des crises de panique avec ou sans agoraphobie.', 'Ce m?dicament est contre-indiqu? en cas d''allergie au produit.'),
(27, 'TOUXISOL Vitamine C', 'Tyrothricine + Acide asco', 'Ce m?dicament est utilis? pour traiter les affections de la bouche et de la gorge.', 'Ce m?dicament est contre-indiqu? en cas d''allergie ?? l''un des constituants et chez l''enfant de moins de 6 ans.'),
(28, 'URIREGUL', 'Fosfomycine trom?tamol', 'Ce m?dicament est utilis? pour traiter les infections urinaires simples chez la femme de moins de 65 ans.', 'La prise de ce m?dicament est contre-indiqu?e en cas d''allergie ?? l''un des constituants et d''insuffisance r?nale.');

-- --------------------------------------------------------

--
-- Structure de la table `posseder`
--

CREATE TABLE IF NOT EXISTS `posseder` (
  `pra_num` int(11) NOT NULL DEFAULT '0',
  `spe_code` varchar(4) NOT NULL DEFAULT '',
  PRIMARY KEY (`pra_num`,`spe_code`),
  KEY `spe_code` (`spe_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `praticien`
--

CREATE TABLE IF NOT EXISTS `praticien` (
  `pra_num` int(11) NOT NULL,
  `pra_nom` varchar(25) DEFAULT NULL,
  `pra_coef_notoriete` float DEFAULT NULL,
  `PRA_ADRESSE` varchar(29) DEFAULT NULL,
  `PRA_CP` int(5) DEFAULT NULL,
  `PRA_VILLE` varchar(19) DEFAULT NULL,
  PRIMARY KEY (`pra_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `praticien`
--

INSERT INTO `praticien` (`pra_num`, `pra_nom`, `pra_coef_notoriete`, `PRA_ADRESSE`, `PRA_CP`, `PRA_VILLE`) VALUES
(1, 'Notini', 290, '114 r Authie', 85000, 'LA ROCHE SUR YON'),
(2, 'Gosselin', 307, '13 r Devon', 41000, 'BLOIS'),
(3, 'Delahaye', 185, '36 av 6 Juin', 25000, 'BESANCON'),
(4, 'Leroux', 172, '47 av Robert Schuman', 60000, 'BEAUVAIS'),
(5, 'Desmoulins', 94, '31 r St Jean', 30000, 'NIMES'),
(6, 'Mouel', 45, '27 r Auvergne', 80000, 'AMIENS'),
(7, 'Desgranges-Lentz', 20, '1 r Albert de Mun', 29000, 'MORLAIX'),
(8, 'Marcouiller', 396, '31 r St Jean', 68000, 'MULHOUSE'),
(9, 'Dupuy', 395, '9 r Demolombe', 34000, 'MONTPELLIER'),
(10, 'Lerat', 257, '31 r St Jean', 59000, 'LILLE'),
(11, 'Mar?ais-Lefebvre', 450, '86Bis r Basse', 67000, 'STRASBOURG'),
(12, 'Boscher', 356, '94 r Falaise', 10000, 'TROYES'),
(13, 'Morel', 379, '21 r Chateaubriand', 75000, 'PARIS'),
(14, 'Guivarch', 114, '4 av G?n Laperrine', 45000, 'ORLEANS'),
(15, 'Bessin-Grosdoit', 222, '92 r Falaise', 6000, 'NICE'),
(16, 'Rossa', 529, '14 av Thi?s', 6000, 'NICE'),
(17, 'Cauchy', 458, '5 av Ste Th?r?se', 11000, 'NARBONNE'),
(18, 'Gaff?', 213, '9 av 1?re Arm?e Fran?aise', 35000, 'RENNES'),
(19, 'Guenon', 175, '98 bd Mar Lyautey', 44000, 'NANTES'),
(20, 'Pr?vot', 151, '29 r Lucien Nelle', 87000, 'LIMOGES'),
(21, 'Houchard', 436, '9 r Demolombe', 49100, 'ANGERS'),
(22, 'Desmons', 281, '51 r Berni?res', 29000, 'QUIMPER'),
(23, 'Flament', 315, '11 r Pasteur', 35000, 'RENNES'),
(24, 'Goussard', 40, '9 r Demolombe', 41000, 'BLOIS'),
(25, 'Desprez', 406, '9 r Vaucelles', 33000, 'BORDEAUX'),
(26, 'Coste', 441, '29 r Lucien Nelle', 19000, 'TULLE'),
(27, 'Lefebvre', 573, '2 pl Wurzburg', 55000, 'VERDUN'),
(28, 'Lem?e', 326, '29 av 6 Juin', 56000, 'VANNES'),
(29, 'Martin', 506, 'B?t A 90 r Bayeux', 70000, 'VESOUL'),
(30, 'Marie', 313, '172 r Caponi?re', 70000, 'VESOUL'),
(31, 'Rosenstech', 366, '27 r Auvergne', 75000, 'PARIS'),
(32, 'Pontavice', 265, '8 r Gaillon', 86000, 'POITIERS'),
(33, 'Leveneur-Mosquet', 184, '47 av Robert Schuman', 64000, 'PAU'),
(34, 'Blanchais', 502, '30 r Authie', 8000, 'SEDAN'),
(35, 'Leveneur', 7, '7 pl St Gilles', 62000, 'ARRAS'),
(36, 'Mosquet', 77, '22 r Jules Verne', 76000, 'ROUEN'),
(37, 'Giraudon', 92, '1 r Albert de Mun', 38100, 'VIENNE'),
(38, 'Marie', 120, '26 r H?rouville', 69000, 'LYON'),
(39, 'Maury', 13, '5 r Pierre Girard', 71000, 'CHALON SUR SAONE'),
(40, 'Dennel', 550, '7 pl St Gilles', 28000, 'CHARTRES'),
(41, 'Ain', 5, '4 r?sid Olympia', 2000, 'LAON'),
(42, 'Chemery', 396, '51 pl Ancienne Boucherie', 14000, 'CAEN'),
(43, 'Comoz', 340, '35 r Auguste Lechesne', 18000, 'BOURGES'),
(44, 'Desfaudais', 71, '7 pl St Gilles', 29000, 'BREST'),
(45, 'Phan', 451, '9 r Clos Caillet', 79000, 'NIORT'),
(46, 'Riou', 193, '43 bd G?n Vanier', 77000, 'MARNE LA VALLEE'),
(47, 'Chubilleau', 202, '46 r Eglise', 17000, 'SAINTES'),
(48, 'Lebrun', 410, '178 r Auge', 54000, 'NANCY'),
(49, 'Goessens', 548, '6 av 6 Juin', 39000, 'DOLE'),
(50, 'Laforge', 265, '5 r?sid Prairie', 50000, 'SAINT LO'),
(51, 'Millereau', 430, '36 av 6 Juin', 72000, 'LA FERTE BERNARD'),
(52, 'Dauverne', 281, '69 av Charlemagne', 21000, 'DIJON'),
(53, 'Vittorio', 356, '3 pl Champlain', 94000, 'BOISSY SAINT LEGER'),
(54, 'Lapasset', 107, '31 av 6 Juin', 52000, 'CHAUMONT'),
(55, 'Plantet-Besnier', 369, '10 av 1?re Arm?e Fran?aise', 86000, 'CHATELLEREAULT'),
(56, 'Chubilleau', 290, '3 r Hastings', 15000, 'AURRILLAC'),
(57, 'Robert', 162, '31 r St Jean', 93000, 'BOBIGNY'),
(58, 'Jean', 375, '114 r Authie', 49100, 'SAUMUR'),
(59, 'Chanteloube', 478, '14 av Thi?s', 13000, 'MARSEILLE'),
(60, 'Lecuirot', 239, 'r?sid St P?res 55 r Pigaci?re', 54000, 'NANCY'),
(61, 'Gandon', 599, '47 av Robert Schuman', 37000, 'TOURS'),
(62, 'Mirouf', 458, '22 r Puits Picard', 74000, 'ANNECY'),
(63, 'Boireaux', 454, '14 av Thi?s', 10000, 'CHALON EN CHAMPAGNE'),
(64, 'Cendrier', 164, '7 pl St Gilles', 12000, 'RODEZ'),
(65, 'Duhamel', 98, '114 r Authie', 34000, 'MONTPELLIER'),
(66, 'Grigy', 285, '15 r M?lingue', 44000, 'CLISSON'),
(67, 'Linard', 486, '1 r Albert de Mun', 81000, 'ALBI'),
(68, 'Lozier', 48, '8 r Gaillon', 31000, 'TOULOUSE'),
(69, 'Dech?tre', 253, '63 av Thi?s', 23000, 'MONTLUCON'),
(70, 'Goessens', 426, '22 r Jean Romain', 40000, 'MONT DE MARSAN'),
(71, 'Lem?nager', 118, '39 av 6 Juin', 57000, 'METZ'),
(72, 'N?e', 72, '39 av 6 Juin', 82000, 'MONTAUBAN'),
(73, 'Guyot', 352, '43 bd G?n Vanier', 48000, 'MENDE'),
(74, 'Chauchard', 552, '9 r Vaucelles', 13000, 'MARSEILLE'),
(75, 'Mabire', 422, '11 r Boutiques', 67000, 'STRASBOURG'),
(76, 'Leroy', 570, '45 r Boutiques', 61000, 'ALENCON'),
(77, 'Guyot', 28, '26 r H?rouville', 46000, 'FIGEAC'),
(78, 'Delposen', 292, '39 av 6 Juin', 27000, 'DREUX'),
(79, 'Rault', 526, '15 bd Richemond', 2000, 'SOISSON'),
(80, 'Renouf', 425, '98 bd Mar Lyautey', 88000, 'EPINAL'),
(81, 'Alliet-Grach', 451, '14 av Thi?s', 7000, 'PRIVAS'),
(82, 'Bayard', 271, '92 r Falaise', 42000, 'SAINT ETIENNE'),
(83, 'Gauchet', 406, '7 r Desmoueux', 38100, 'GRENOBLE'),
(84, 'Bobichon', 218, '219 r Caponi?re', 9000, 'FOIX'),
(85, 'Duchemin-Laniel', 265, '130 r St Jean', 33000, 'LIBOURNE'),
(86, 'Laurent', 496, '34 r Demolombe', 53000, 'MAYENNE');

-- --------------------------------------------------------

--
-- Structure de la table `proposer`
--

CREATE TABLE IF NOT EXISTS `proposer` (
  `v_num` int(11) NOT NULL DEFAULT '0',
  `ech_num` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`v_num`,`ech_num`),
  KEY `ech_num` (`ech_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `region`
--

CREATE TABLE IF NOT EXISTS `region` (
  `REG_CODE` varchar(2) NOT NULL,
  `SEC_CODE` varchar(1) DEFAULT NULL,
  `REG_NOM` varchar(26) DEFAULT NULL,
  PRIMARY KEY (`REG_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `region`
--

INSERT INTO `region` (`REG_CODE`, `SEC_CODE`, `REG_NOM`) VALUES
('AL', 'E', 'Alsace Lorraine'),
('AQ', 'S', 'Aquitaine'),
('AU', 'P', 'Auvergne'),
('BG', 'O', 'Bretagne'),
('BN', 'O', 'Basse Normandie'),
('BO', 'E', 'Bourgogne'),
('CA', 'N', 'Champagne Ardennes'),
('CE', 'P', 'Centre'),
('FC', 'E', 'Franche Comt?'),
('HN', 'N', 'Haute Normandie'),
('IF', 'P', 'Ile de France'),
('LG', 'S', 'Languedoc'),
('LI', 'P', 'Limousin'),
('MP', 'S', 'Midi Pyr?n?e'),
('NP', 'N', 'Nord Pas de Calais'),
('PA', 'S', 'Provence Alpes Cote d''Azur'),
('PC', 'O', 'Poitou Charente'),
('PI', 'N', 'Picardie'),
('PL', 'O', 'Pays de Loire'),
('RA', 'E', 'Rhone Alpes'),
('RO', 'S', 'Roussilon'),
('VD', 'O', 'Vend?e');

-- --------------------------------------------------------

--
-- Structure de la table `specialite`
--

CREATE TABLE IF NOT EXISTS `specialite` (
  `spe_code` varchar(4) NOT NULL,
  `spe_libelle` varchar(25) DEFAULT NULL,
  `code_dip` int(3) NOT NULL,
  PRIMARY KEY (`spe_code`),
  KEY `code_dip` (`code_dip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `specialite`
--

INSERT INTO `specialite` (`spe_code`, `spe_libelle`, `code_dip`) VALUES
('IH', 'liberal', 6),
('MH', 'M?decin Hospitalier', 1),
('MV', 'M?decine de Ville', 2),
('PH', 'Pharmacien Hospitalier', 3),
('PO', 'Pharmacien Officine', 4),
('PS', 'Personnel de sant?', 5);

-- --------------------------------------------------------

--
-- Structure de la table `tva`
--

CREATE TABLE IF NOT EXISTS `tva` (
  `id_tva` int(1) NOT NULL,
  `taux_tva` float NOT NULL,
  PRIMARY KEY (`id_tva`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `tva`
--

INSERT INTO `tva` (`id_tva`, `taux_tva`) VALUES
(1, 1.2),
(2, 1.1),
(3, 1.055),
(4, 1.021);

-- --------------------------------------------------------

--
-- Structure de la table `visite`
--

CREATE TABLE IF NOT EXISTS `visite` (
  `v_num` int(11) NOT NULL AUTO_INCREMENT,
  `v_date` date DEFAULT NULL,
  `v_lieu` varchar(25) DEFAULT NULL,
  `v_description` varchar(90) DEFAULT NULL,
  `vis_NumSecu` int(11) DEFAULT NULL,
  `pra_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`v_num`),
  KEY `vis_NumSecu` (`vis_NumSecu`),
  KEY `pra_num` (`pra_num`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `visite`
--

INSERT INTO `visite` (`v_num`, `v_date`, `v_lieu`, `v_description`, `vis_NumSecu`, `pra_num`) VALUES
(1, '0000-00-00', 'Hopital ou clinique', 'M?decin curieux, ? recontacer en d?cembre pour r?union', NULL, NULL),
(2, '0000-00-00', 'cabinet', 'Changement de direction, red?finition de la politique m?dicamenteuse, recours au g?n?rique', NULL, NULL),
(3, '0000-00-00', 'Hopital ou clinique', 'RAS\r\nChangement de tel : 05 89 89 89 89', NULL, NULL),
(4, NULL, 'Pharmacie', NULL, NULL, NULL),
(5, NULL, 'Centre param?dical', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `visiteur`
--

CREATE TABLE IF NOT EXISTS `visiteur` (
  `vis_NumSecu` int(11) NOT NULL AUTO_INCREMENT,
  `vis_nom` varchar(25) DEFAULT NULL,
  `vis_adresse` varchar(31) DEFAULT NULL,
  `vis_cp` varchar(5) DEFAULT NULL,
  `vis_ville` varchar(20) DEFAULT NULL,
  `vis_dateEmbauche` date DEFAULT NULL,
  `vis_region_code` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`vis_NumSecu`),
  KEY `vis_region_code` (`vis_region_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69 ;

--
-- Contenu de la table `visiteur`
--

INSERT INTO `visiteur` (`vis_NumSecu`, `vis_nom`, `vis_adresse`, `vis_cp`, `vis_ville`, `vis_dateEmbauche`, `vis_region_code`) VALUES
(1, 'Villechalane', '8 cours Lafontaine', '29000', 'BREST', '0000-00-00', 'AL'),
(2, 'Andre', '1 r Aimon de Chiss?e', '38100', 'GRENOBLE', '0000-00-00', 'BG'),
(3, 'Bedos', '1 r B?n?dictins', '65000', 'TARBES', '0000-00-00', 'LI'),
(4, 'Tusseau', '22 r Renou', '86000', 'POITIERS', '0000-00-00', 'LI'),
(5, 'Bentot', '11 av 6 Juin', '67000', 'STRASBOURG', '0000-00-00', 'CE'),
(6, 'Bioret', '1 r Linne', '35000', 'RENNES', '0000-00-00', 'IF'),
(7, 'Bunisset', '10 r Nicolas Chorier', '85000', 'LA ROCHE SUR YON', '0000-00-00', 'BN'),
(8, 'Bunisset', '1 r Lionne', '49100', 'ANGERS', '0000-00-00', 'IF'),
(9, 'Cacheux', '114 r Authie', '34000', 'MONTPELLIER', '0000-00-00', 'AL'),
(10, 'Cadic', '123 r Caponi?re', '41000', 'BLOIS', '0000-00-00', 'FC'),
(11, 'Charoze', '100 pl G?ants', '33000', 'BORDEAUX', '0000-00-00', 'PA'),
(12, 'Clepkens', '12 r F?d?rico Garcia Lorca', '13000', 'MARSEILLE', '0000-00-00', 'NP'),
(13, 'Cottin', '36 sq Capucins', '5000', 'GAP', '0000-00-00', 'PC'),
(14, 'Daburon', '13 r Champs Elys?es', '6000', 'NICE', '0000-00-00', 'PA'),
(15, 'De', '13 r Charles Peguy', '10000', 'TROYES', '0000-00-00', 'RA'),
(16, 'Debelle', '181 r Caponi?re', '88000', 'EPINAL', '0000-00-00', 'HN'),
(17, 'Debelle', '134 r Stalingrad', '44000', 'NANTES', '0000-00-00', 'FC'),
(18, 'Debroise', '2 av 6 Juin', '70000', 'VESOUL', '0000-00-00', 'RA'),
(19, 'Desmarquest', '14 r F?d?rico Garcia Lorca', '54000', 'NANCY', '0000-00-00', 'PA'),
(20, 'Desnost', '16 r Barral de Montferrat', '55000', 'VERDUN', '0000-00-00', 'PI'),
(21, 'Dudouit', '18 quai Xavier Jouvin', '75000', 'PARIS', '0000-00-00', 'PA'),
(22, 'Duncombe', '19 av Alsace Lorraine', '9000', 'FOIX', '0000-00-00', 'NP'),
(23, 'Enault-Pascreau', '25B r Stalingrad', '40000', 'MONT DE MARSAN', '0000-00-00', 'MP'),
(24, 'Eynde', '3 r Henri Moissan', '76000', 'ROUEN', '0000-00-00', 'MP'),
(25, 'Finck', 'rte Montreuil Bellay', '74000', 'ANNECY', '0000-00-00', 'AU'),
(26, 'Fr?mont', '4 r Jean Giono', '69000', 'LYON', '0000-00-00', 'CE'),
(27, 'Gest', '30 r Authie', '46000', 'FIGEAC', '0000-00-00', 'PA'),
(28, 'Gheysen', '32 bd Mar Foch', '75000', 'PARIS', '0000-00-00', 'AQ'),
(29, 'Girard', '31 av 6 Juin', '80000', 'AMIENS', '0000-00-00', 'NP'),
(30, 'Gombert', '32 r Emile Gueymard', '56000', 'VANNES', '0000-00-00', 'LI'),
(31, 'Guindon', '40 r Mar Montgomery', '87000', 'LIMOGES', '0000-00-00', 'AL'),
(32, 'Guindon', '44 r Picoti?re', '19000', 'TULLE', '0000-00-00', 'LG'),
(33, 'Igigabel', '33 gal Arlequin', '94000', 'CRETEIL', '0000-00-00', 'PC'),
(34, 'Jourdren', '34 av Jean Perrot', '15000', 'AURRILLAC', '0000-00-00', 'AU'),
(35, 'Juttard', '34 cours Jean Jaur?s', '8000', 'SEDAN', '0000-00-00', 'BG'),
(36, 'Labour?-Morel', '38 cours Berriat', '52000', 'CHAUMONT', '0000-00-00', 'FC'),
(37, 'Landr?', '4 av G?n Laperrine', '59000', 'LILLE', '0000-00-00', 'PA'),
(38, 'Langeard', '39 av Jean Perrot', '93000', 'BAGNOLET', '0000-00-00', 'CA'),
(39, 'Lanne', '4 r Bayeux', '30000', 'NIMES', '0000-00-00', 'PA'),
(40, 'Le', '4 av Beauvert', '68000', 'MULHOUSE', '0000-00-00', 'CE'),
(41, 'Le', '39 r Raspail', '53000', 'LAVAL', '0000-00-00', 'HN'),
(42, 'Leclercq', '11 r Quinconce', '18000', 'BOURGES', '0000-00-00', 'RO'),
(43, 'Lecornu', '4 bd Mar Foch', '72000', 'LA FERTE BERNARD', '0000-00-00', 'HN'),
(44, 'Lecornu', '4 r Abel Servien', '25000', 'BESANCON', '0000-00-00', 'PI'),
(45, 'Lejard', '4 r Anthoard', '82000', 'MONTAUBAN', '0000-00-00', 'HN'),
(46, 'Lesaulnier', '47 r Thiers', '57000', 'METZ', '0000-00-00', 'BN'),
(47, 'Letessier', '5 chem Capuche', '27000', 'EVREUX', '0000-00-00', 'CE'),
(48, 'Loirat', 'Les P?chers cit? Bourg la Croix', '45000', 'ORLEANS', '0000-00-00', 'IF'),
(49, 'Maffezzoli', '5 r Chateaubriand', '2000', 'LAON', '0000-00-00', 'FC'),
(50, 'Mancini', '5 r D''Agier', '48000', 'MENDE', '0000-00-00', 'CE'),
(51, 'Marcouiller', '7 pl St Gilles', '91000', 'ISSY LES MOULINEAUX', '0000-00-00', 'AQ'),
(52, 'Michel', '5 r Gabriel P?ri', '61000', 'FLERS', '0000-00-00', 'RO'),
(53, 'Montecot', '6 r Paul Val?ry', '17000', 'SAINTES', '0000-00-00', 'VD'),
(54, 'Notini', '5 r Lieut Chabal', '60000', 'BEAUVAIS', '0000-00-00', 'HN'),
(55, 'Onfroy', '5 r Sidonie Jacolin', '37000', 'TOURS', '0000-00-00', 'AL'),
(56, 'Pascreau', '57 bd Mar Foch', '64000', 'PAU', '0000-00-00', 'PI'),
(57, 'Pernot', '6 r Alexandre 1 de Yougoslavie', '11000', 'NARBONNE', '0000-00-00', 'BN'),
(58, 'Perrier', '6 r Aubert Dubayet', '71000', 'CHALON SUR SAONE', '0000-00-00', 'PI'),
(59, 'Petit', '7 r Ernest Renan', '50000', 'SAINT LO', '0000-00-00', 'LI'),
(60, 'Piquery', '9 r Vaucelles', '14000', 'CAEN', '0000-00-00', 'AL'),
(61, 'Quiquandon', '7 r Ernest Renan', '29000', 'QUIMPER', '0000-00-00', 'BG'),
(62, 'Retailleau', '88Bis r Saumuroise', '39000', 'DOLE', '0000-00-00', 'AL'),
(63, 'Retailleau', '32 bd Ayrault', '23000', 'MONTLUCON', '0000-00-00', 'AU'),
(64, 'Souron', '7B r Gay Lussac', '21000', 'DIJON', '0000-00-00', 'BN'),
(65, 'Tiphagne', '7B r Gay Lussac', '62000', 'ARRAS', '0000-00-00', 'BG'),
(66, 'Tr?het', '7D chem Barral', '12000', 'RODEZ', '0000-00-00', 'RA'),
(67, 'Tusseau', '63 r Bon Repos', '28000', 'CHARTRES', '0000-00-00', 'AU'),
(68, 'swiss', '', '', '', '0000-00-00', 'PA');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `appartenir`
--
ALTER TABLE `appartenir`
  ADD CONSTRAINT `appartenir_ibfk_1` FOREIGN KEY (`vis_NumSecu`) REFERENCES `visiteur` (`vis_NumSecu`),
  ADD CONSTRAINT `appartenir_ibfk_2` FOREIGN KEY (`REG_CODE`) REFERENCES `region` (`REG_CODE`);

--
-- Contraintes pour la table `echantillonmedic`
--
ALTER TABLE `echantillonmedic`
  ADD CONSTRAINT `id_tva` FOREIGN KEY (`id_tva`) REFERENCES `tva` (`id_tva`),
  ADD CONSTRAINT `echantillonmedic_ibfk_1` FOREIGN KEY (`med_depotLegal`) REFERENCES `medicament` (`med_depotLegal`);

--
-- Contraintes pour la table `posseder`
--
ALTER TABLE `posseder`
  ADD CONSTRAINT `posseder_ibfk_1` FOREIGN KEY (`pra_num`) REFERENCES `praticien` (`pra_num`),
  ADD CONSTRAINT `posseder_ibfk_2` FOREIGN KEY (`spe_code`) REFERENCES `specialite` (`spe_code`);

--
-- Contraintes pour la table `proposer`
--
ALTER TABLE `proposer`
  ADD CONSTRAINT `proposer_ibfk_1` FOREIGN KEY (`v_num`) REFERENCES `visite` (`v_num`),
  ADD CONSTRAINT `proposer_ibfk_2` FOREIGN KEY (`ech_num`) REFERENCES `echantillonmedic` (`ech_num`);

--
-- Contraintes pour la table `specialite`
--
ALTER TABLE `specialite`
  ADD CONSTRAINT `specialite_ibfk_1` FOREIGN KEY (`code_dip`) REFERENCES `diplome` (`code_dip`);

--
-- Contraintes pour la table `visite`
--
ALTER TABLE `visite`
  ADD CONSTRAINT `visite_ibfk_1` FOREIGN KEY (`vis_NumSecu`) REFERENCES `visiteur` (`vis_NumSecu`),
  ADD CONSTRAINT `visite_ibfk_2` FOREIGN KEY (`pra_num`) REFERENCES `praticien` (`pra_num`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
