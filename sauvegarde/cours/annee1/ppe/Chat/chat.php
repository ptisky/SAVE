<?php
// On prolonge la session
session_start();
// On teste si la variable de session existe et contient une valeur
if(empty($_SESSION['login'])) 
{
  // Si inexistante ou nulle, on redirige vers le formulaire de login
  header('Location: index.php');
  exit();
}
  ?>
<html>
	<head>
		<meta charset="UTF-8">

		<link rel="stylesheet" href="css/chat.css"/>
		<link rel="stylesheet" href="css/bootstrap.css"/>

	</head>

<body>
	<table id="body"> <!-- Premier tableau, met en forme la page -->
  
  <?php
	$jour = date('d');
	$mois = date ('m');
	$annee = date ('y');
	$heure = date('H');
	$minute = date('i');

	echo "<FONT COLOR='3498db'>";
    // Ici on est bien loggué, on affiche un message
	
	echo  '<p> Bienvenue ' . $_SESSION['login'] . '!<br> Nous sommes le <strong>' . $jour . '/' . $mois . '/' . $annee . '</strong> et il est <strong>' . $heure. ' h ' . $minute . '</strong>!</p>';
  ?>
  
		<a href="deconnexion.php">deconnexion</a>

		<td id="titre">CHAT BTS</td>


	<tr>
 
		<td style="height:500px">
 
			<div id="chat_aff"></div>
 
		</td>
 
	</tr>
 
	<tr>
 
		<td id="form" valign="top">
 
			<table id="form2"> <!-- deuxieme tableau, met en forme le chat -->
			
				<form id="formulaire" name="validation" onKeyPress="if(event.keyCode == 13) validerForm();"> <!-- Valider le formulaire avec la touche entré -->
				
				<tr>

					<td><!-- emplacement pseudo --> 
						<label style="font-family:Comic Sans MS;"><FONT COLOR="3498db">Pseudo</label> <!-- Le label "text" -->
					</td>

					<td style="width:60%"><!-- emplacement message --> 
						<label for="message" style="font-family:Comic Sans MS;"><FONT COLOR="3498db">Message</label> <!-- Le label "message" -->
					</td>
 
				</tr>
				<tr>

					<td>  
						<FONT COLOR="#046380"><?php echo $_SESSION['login']; ?> <!-- Le pseudo de la session -->
						<!-- <?php // echo "<input id='name' type='hidden' maxlength='250'  value='" . $_SESSION['login'] ."'/>"?> valeur du login --> 
					</td>
					<td>
						<input id="message" type="text" maxlength="250" name="message" value='' onblur ="if (this.value=='') {this.value='tapez votre message..'}" onFocus="if (this.value=='tapez votre message..') {this.value=''}"> <!-- le message tapé par l'utilisateur --> 
					</td>
    
					<td>
						<button id="submit"  class="button">Envoyer</button>  <!-- Boutton pour envoyer le message -->  
					</td>
					
				</tr>
				
				</form>
				<SCRIPT>document.validation.message.focus();</SCRIPT>


				
			</table>
 
		</td>
 
	</tr>

	</table>
</body>

<script src="js/jquery.js"></script>
<script src="js/reload.js"></script>

</html>