<table id="table_message"> <!-- tableau ou sera affiché le chat -->

<?php

while($don = $message->fetch()) //début de boucle (fin ligne 52)
{
    if(pair($don['ID']))
    {
        $color = "";
    }
    else
    {
        $color = "#333333";
    }
	
	$couleurs = array("3498db","91D51B","RED");
	$monrang = array("Membre","VIP","Admin");
	
	$don['message'] = str_replace( ":)", '<img src="emoticons/smile.png" alt=":)" class="smile">', $don['message'] );
	$don['message'] = str_replace( "zzz", '<img src="emoticons/zzz.png" alt="zzz" class="zzz">', $don['message'] );
	$don['message'] = str_replace( ":(", '<img src="emoticons/serious.png" alt=":(" class="serious">', $don['message'] );
	$don['message'] = str_replace( "aille!", '<img src="emoticons/aille.png" alt="aille!" class="aille">', $don['message'] );
	$don['message'] = str_replace( ":o", '<img src="emoticons/omg.png" alt=":o" class="omg">', $don['message'] );
	$don['message'] = str_replace( ":p", '<img src="emoticons/na.png" alt=":p" class="na">', $don['message'] );
	$don['message'] = str_replace( ":brule:", '<img src="emoticons/burnt.png" alt="brule" class="burnt">', $don['message'] );
	$don['message'] = str_replace( ":agent:", '<img src="emoticons/confident.png" alt=":agent:" class="confident">', $don['message'] );
	$don['message'] = str_replace( ":huum:", '<img src="emoticons/dark mood.png" alt=":)" class="huum">', $don['message'] );
	$don['message'] = str_replace( ":disapointed:", '<img src="emoticons/disapointed.png" alt=":disapointed:" class="disapointed">', $don['message'] );
?>

<tr style="background-color:<?php echo $color; ?>">

    <td class="info_message" valign="top"> <!-- Partie qui concerne l'affichage du pseudo et du rang dans le chat -->
	
		<span style="font-size:small"><FONT COLOR="#3498db"><?php echo "De ".$don['pseudo'];?></span><br> <!-- Le pseudo -->
		<span style="font-size:small"><FONT COLOR="<?php echo $couleurs[$don["rang"]];?>"><?php echo $monrang[$don["rang"]];?></span> <!-- Le rang -->
		
    </td>
	
    <td class="message" > <!-- Partie ou les messages serons affichés -->
	
		<div class="message2" >
	
			<FONT COLOR="<?php echo $couleurs[$don["rang"]];?>"><?php echo $don['message'];?>
	
		</div>    
		
    </td>
	
	<td>
	
	<span style="font-size:small"><FONT COLOR="3498db">	 	
	<FONT COLOR="808080"><?php echo getRelativeTime($don['Date']);?> <!-- La temps d'envoie des messages -->
	
	</td>
	
</tr>
 
<?php
 
} //fin de boucle

?>

</table>