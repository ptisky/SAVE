<?php
//connexion bdd
function bdd()
{
return $db = new PDO('mysql:host=localhost;dbname=chat', 'root', '');;
}
//ajouter un message
function ajout_message($bdd,$pseudo,$message,$rang)
{
    $req = $bdd->prepare("INSERT INTO message(pseudo,message,rang,Date) VALUES(:pseudo,:message,:rang,NOW())");
    $req->execute(array("pseudo"=>$pseudo,"message"=>$message,"rang"=>$rang));
}
//afficher les messages
function message($bdd)
{
    $req = $bdd->query("SELECT * FROM message ORDER BY Date DESC");
     
    return $req;
}
//supprime un message au bout de 30 minutes
function expire_message($bdd)
{
     
    $req = $bdd->query("DELETE FROM message WHERE Date < DATE_SUB(NOW(), INTERVAL 30 MINUTE)");
     
}
 //calcule le pair/impaire -> utile pour le chat afficher 1 couleurs sur 2 
function pair($nombre)
{
    if ($nombre%2 == 0) return true;
    else return false;
}
 

 function getRelativeTime($date) {
    // Déduction de la date donnée à la date actuelle
    $time = time() - strtotime($date); 
 
    // Calcule si le temps est passé ou à venir
    if ($time > 60) {
        $when = "il y a";
    } else if ($time < 1) {
        $when = "maintenant";
    } else {
        return "maintenant";
    }
    $time = abs($time); 
 
    // Tableau des unités et de leurs valeurs en secondes
    $times = array( 31104000 =>  'an{s}',       // 12 * 30 * 24 * 60 * 60 secondes
                    2592000  =>  'mois',        // 30 * 24 * 60 * 60 secondes
                    86400    =>  'jour{s}',     // 24 * 60 * 60 secondes
                    60     =>  'heure{s}',    // 60 * 60 secondes
                    60      =>  'minute{s}',);    // 1 minute    
 
    foreach ($times as $minute => $unit) {
        // Calcule le delta entre le temps et l'unité donnée
        $delta = round($time / $minute); 
 
        // Si le delta est supérieur à 1
        if ($delta >= 1) {
            // L'unité est au singulier ou au pluriel ?
            if ($delta == 1) {
                $unit = str_replace('{s}', '', $unit);
            } else {
                $unit = str_replace('{s}', 's', $unit);
            }
            // Retourne la chaine adéquate
            return $when." ".$delta." ".$unit;
        }
    }
}
?>