package bts;

import java.util.Date;

public class ManutARisque extends Manutentionnaire implements ARisque{
	public ManutARisque(String mNom, String mPrenom, int mAge, Date mdate, int pNbHeure)
	{
		super(mNom, mPrenom, mAge, mdate, pNbHeure);
	}
	
	public double getPrimeRisque()
	{
		double primeRisque=550.0;
		return primeRisque;
	}
	
	@Override
	double calculerSalaire() {
		return super.calculerSalaire()+this.getPrimeRisque();
	}
}
