package bts;

import java.util.ArrayList;

public class Personnel {
	private int nbEmployes;
	private ArrayList<Employe> employer= new ArrayList<Employe>();
	final int MAXEMPLOYE = 50;
	
	public Personnel()
	{
		
	}
	
	public void ajouterEmploye(Employe e)
	{
		this.employer.add(e);
	}
	
	public double salaireMoyen()
	{
		double temp=0;
		for(Employe e : employer)
		{
			temp = temp + e.calculerSalaire();
		}
		return temp/employer.size();
	}
	
	public void afficherSalaire(Employe e)
	{
		double salaire= e.calculerSalaire();
		System.out.println("je gagne "+salaire+" euros");
	}
	
	
	
}
