package bts;

import java.util.Date;

public class Representant extends Commercial{

	final double POURCENT_REPRESENTANT = 1.1599;
	final int BONUS_REPRESENTANT = 500;
	
	public Representant(String rnom, String rprenom, int rage, Date rdate, double rchiffreAffaire)
	{
		super(rnom, rprenom, rage, rdate, rchiffreAffaire);
	}

	@Override
	double calculerSalaire() {
		return (POURCENT_REPRESENTANT*this.chiffreAffaire)+BONUS_REPRESENTANT;
	}
	
	public String getTitre()
	{
		String temp = getClass().getName();
		temp = temp.substring(temp.indexOf(".")+1);
		return temp;
	}
	
	
}
