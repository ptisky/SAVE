package bts;

import java.util.Date;

abstract public class Employe {
	
	protected String nom;
	protected String prenom;
	protected Date dateEmbauche;
	protected int age;
	private Personnel staff;
	
	public Employe()
	{
		
	}
	
	public Employe(String eNom, String ePrenom, int eAge, Date edateEmbauche)
	{
		this.nom=eNom;
		this.prenom=ePrenom;
		this.age=eAge;
		
	}
	
	public Date getDateEntree()
	{
		return this.dateEmbauche;
	}
	
	abstract double calculerSalaire();
	
	public String getTitre()
	{
		String temp = getClass().getName();
		temp = temp.substring(temp.indexOf(".")+1);
		return temp;
	}
	
	public String getNom()
	{
		String temp = getClass().getName();
		temp = temp.substring(temp.indexOf(".")+1);
		return temp+this.nom+this.prenom;
	}
	
	
	
	
	

}
