package bts1.slam;

public class Personne {
	
	private String nom;
	private String prenom;
	private int age;
	
	/* constructeur par d�faut (sans arguments, ou sans param�tres) et explicite */	
	public Personne() {
	
	}
	// constructeur � 3 arguments 
	public Personne(String pNom, String pPrenom, int pAge) {
		this.nom = pNom;
		this.prenom = pPrenom;
		this.age = pAge;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public void declinerIdentite()
	{
		System.out.println("je m'appelle "+ this.prenom + " "+ this.nom+" et j'ai "+this.age +" ans ! ");
	}


	
	

}
