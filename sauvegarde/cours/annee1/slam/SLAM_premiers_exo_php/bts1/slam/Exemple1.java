package bts1.slam;

public class Exemple1 {

	public static void main(String[] args) {
		
		Personne p= new Personne();
		p.setNom("ABDELMOULA");
		p.setPrenom("Zoubeida");
		p.setAge(8);
		
		p.declinerIdentite();
		
		Personne p2 = new Personne("Le Meur", "Fred", 10);
		p2.declinerIdentite();

	}

}
