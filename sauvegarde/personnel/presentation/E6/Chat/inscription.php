<?php
//type du document
header('Content-type: text/html; charset=UTF-8');

//rend la variable message null a l'arriver de la page
$message = null;

$pseudo = filter_input(INPUT_POST, 'pseudo');
$pass = filter_input(INPUT_POST, 'pass');

//on regarde si les champs sont vides
if (isset($pseudo,$pass)) 
{   
    $pseudo = trim($pseudo) != '' ? $pseudo : null;
    $pass = trim($pass) != '' ? $pass : null;
   
    if(isset($pseudo,$pass)) 
    {//connexion bdd
    $hostname = "localhost";
    $database = "chat";
    $username = "root";
    $password = "";
    

    
//afficher les erreurs
    $pdo_options[PDO::ATTR_EMULATE_PREPARES] = false;
    

    $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
    

    $pdo_options[PDO::MYSQL_ATTR_INIT_COMMAND] = "SET NAMES utf8";
    
    /* Connexion */
    try
    {
      $connect = new PDO('mysql:host='.$hostname.';dbname='.$database, $username, $password, $pdo_options);
    }
    catch (PDOException $e)
    {
      exit('problème de connexion à la base');
    }
           
    $requete = "SELECT count(*) FROM membres WHERE pseudo = ?";
    
    try
    {
	//preparation de la requete d'insertion
      $req_prep = $connect->prepare($requete);   

      $req_prep->execute(array(0=>$pseudo));
      
      $resultat = $req_prep->fetchColumn();
      
      if ($resultat == 0) 
      {//requete d'insertion
        $insertion = "INSERT INTO membres(pseudo,pass,date_enregistrement,rang) VALUES(:nom, :password, NOW(),0)";
        
        $insert_prep = $connect->prepare($insertion);
        
        $inser_exec = $insert_prep->execute(array(':nom'=>$pseudo,':password'=>$pass));
        
        if ($inser_exec === true) 
        {//on démare une session
          if (!session_id()) session_start();
          $_SESSION['login'] = $pseudo;
          //message de confirmation
          $message = 'Votre inscription est enregistrée.';
		  header('Location: index.php');
        }   
      }
      else
      {  //si le login deja utulisé
        $message = 'Ce pseudo est déjà utilisé, changez-le.';
      }
    }
    catch (PDOException $e)
    {//si il y a un probleme d'insertion
      $message = 'Problème dans la requête d\'insertion';
    }	
  }
  else 
  { //si un des champs pas remplie
    $message = 'Les champs Pseudo et Mot de passe doivent être remplis.';
  }
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Index</title>
	
    <link rel="stylesheet" href="css/css_co.css">


  </head>

	<body>
		<div class="login">
			<h1>Inscription</h1>
			<form method="post">
				<input type="text" name="pseudo" placeholder="Nom de compte" required="required" /> 
				<input type="password" name="pass" placeholder="Mot de passe" required="required" />
				<button type="submit" class="btn btn-primary btn-block btn-large">GO</button>
		
				<a href="index.php">Déjà un compte ? </A>
				<p id = "message"><?= $message?:'' ?></p>
			</form>
		</div>

	</body>
</html>