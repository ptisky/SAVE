package bts;

import java.util.Date;

public abstract class Commercial extends Employe{

	
	protected double chiffreAffaire;
	
	public Commercial(String cnom, String cprenom, int cage, Date cdate, double cchiffreAffaire)
	{
		super(cnom, cprenom, cage, cdate);
		this.chiffreAffaire=cchiffreAffaire;
	}
	
	public double getChiffreAffaire()
	{
		return this.chiffreAffaire;
	}
	

	
	

}
