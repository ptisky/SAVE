<body>

<!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">
                    <i class="fa fa-paper-plane-o" aria-hidden="true"></i><span class = "anautomyHeader">&nbsp; FollowView</span>
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
			<?php if(empty($_SESSION['login'])){
			echo '
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <div class = "headerButton">
                                <a href="inscription.php" class="ghost-button-signup">inscription</a>
                            </div>
                    </li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Se connecter <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<form id="connect" method="post" action="../modele/verrif_co.php" role="form">
								<input type="text" name="pseudo" placeholder="Nom de compte" required="required" /> <br>		
								<input type="password" name="pass" placeholder="Mot de passe" required="required" /><br>
								<button type="submit" class="btn btn-primary" name="button3">Valider</button>	
							</form>
						</ul>
					</li>
                </ul>

            </div>';}else{
			
			echo'<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <div class = "headerButton">
						<font color="red">'.$_SESSION['login'].'</font>
                               <a href="../modele/deconnection.php" class="ghost-button-signup">deconnection</a>
                            </div>
                    </li>
                </ul>
            </div>';
			}
			
			?>
			
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>




    <!-- Navigation -->
    <div class = "header2">
    <nav class="navbar navbar-inverse navbar-static-top mainNav" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="index.php" class = "navText">Acceuil</a>
                    </li>
                    <li>
                        <a href="anime.php" class = "navText">Animé</a>
                    </li>
                    <li>
                        <a href="film.php" class = "navText">Film</a>
                    </li>
                    <li>
                        <a href="serie.php" class = "navText">Serie</a>
                    </li>
                    <li>
                        <a href="ma_liste.php" class = "navText">Ma liste</a>
                    </li>
                    <li>
                        <a href="mon_compte.php" class = "navText">Mon profil</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
</div>