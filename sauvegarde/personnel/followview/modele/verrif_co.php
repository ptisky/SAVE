<?php
//type du document
header('Content-type: text/html; charset=UTF-8');
//test login/mdp 
$message = null;
$pseudo = filter_input(INPUT_POST, 'pseudo');
$pass = filter_input(INPUT_POST, 'pass');


if (isset($pseudo,$pass)) 
{  
    $pseudo = trim($pseudo) != '' ? $pseudo : null;
    $pass = trim($pass) != '' ? $pass : null;
  
  if(isset($pseudo,$pass)) 
  {//test login/mdp 
    $hostname = "localhost";
    $database = "followview";
    $username = "root";
    $password = "";
//afficher les erreurs
    $pdo_options[PDO::ATTR_EMULATE_PREPARES] = false;
    $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
    $pdo_options[PDO::MYSQL_ATTR_INIT_COMMAND] = "SET NAMES utf8";
    
    /* Connexion */
    try
    {
      $connect = new PDO('mysql:host='.$hostname.';dbname='.$database, $username, $password, $pdo_options);
    }
    catch (PDOException $e)
    {
      exit('problème de connexion à la base');
    }    
    
    $requete = "SELECT pseudo, mdp FROM membre WHERE pseudo = :pseudo AND mdp = :mdp";  
    
    try
    {//preparation de la requete d'insertion
      $req_prep = $connect->prepare($requete);
      $req_prep->execute(array(':pseudo'=>$pseudo,':mdp'=>$pass));
      $resultat = $req_prep->fetchAll(); 
      
      $nb_result = count($resultat);
      
      if ($nb_result == 1)
      {//on démare une session
        if (!session_id()) session_start();
        $_SESSION['login'] = $pseudo;
            //message de confirmation
        $message = 'Bonjour '.htmlspecialchars($_SESSION['login']).', vous êtes connecté';
      }
      else if ($nb_result > 1)
      {// si il y a plus de 1 login identique dans les tables
        $message = 'Problème de d\'unicité dans la table';
      }
      else
      {  //si mdp et/ou login introuvable
        $message = 'Le pseudo ou le mot de passe sont incorrect';
      }
    }
    catch (PDOException $e)
    {//si une erreur dans la requete 
      $message = 'Problème dans la requête de sélection';
    }	
  }
  else 
  {//si un des champ n'est pas remplis
    $message = 'Les champs Pseudo et Mot de passe doivent être remplis.';
  }
}
$message;
header('Location: ../vue/index.php');
?>