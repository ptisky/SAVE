<?php
$message = null;
$pseudo = filter_input(INPUT_POST, 'pseudo');
$pass = filter_input(INPUT_POST, 'pass');
$pass_verif = filter_input(INPUT_POST, 'pass_verif');
$nom = filter_input(INPUT_POST, 'nom');
$prenom = filter_input(INPUT_POST, 'prenom');
$mail = filter_input(INPUT_POST, 'mail');
$date_naissance = filter_input(INPUT_POST, 'date_naissance');

//on regarde si les champs sont vides
if (isset($pseudo,$pass)) 
{   
    $pseudo = trim($pseudo) != '' ? $pseudo : null;
    $pass = trim($pass) != '' ? $pass : null;
   
    if(isset($pseudo,$pass)) 
    {//connexion bdd
    $hostname = "localhost";
    $database = "followview";
    $username = "root";
    $password = "";
    

    
//afficher les erreurs
    $pdo_options[PDO::ATTR_EMULATE_PREPARES] = false;
    

    $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
    

    $pdo_options[PDO::MYSQL_ATTR_INIT_COMMAND] = "SET NAMES utf8";
    
    /* Connexion */
    try
    {
      $connect = new PDO('mysql:host='.$hostname.';dbname='.$database, $username, $password, $pdo_options);
    }
    catch (PDOException $e)
    {
      exit('problème de connexion à la base');
    }
           
    $requete = "SELECT count(*) FROM membre WHERE pseudo = ?";
    
    try
    {
	//preparation de la requete d'insertion
      $req_prep = $connect->prepare($requete);   

      $req_prep->execute(array(0=>$pseudo));
      
      $resultat = $req_prep->fetchColumn();
      
      if ($resultat == 0) 
      {//requete d'insertion
        $insertion = "INSERT INTO membre(pseudo,mdp,rang,date_enregistrement,nom,prenom,mail,date_naissance) VALUES(:pseudo, :mdp,1 ,NOW() ,:nom ,:prenom ,:mail ,:date_naissance)";
        
        $insert_prep = $connect->prepare($insertion);
        
        $inser_exec = $insert_prep->execute(array(':pseudo'=>$pseudo,':mdp'=>$pass,':nom'=>$nom,':prenom'=>$prenom,':mail'=>$mail,':date_naissance'=>$date_naissance));
        
        if ($inser_exec === true) 
        {//on démare une session
          if (!session_id()) session_start();
          $_SESSION['login'] = $pseudo;
          //message de confirmation
          $message = 'Votre inscription est enregistrée.';
		  header('Location: index.php');
        }   
      }
      else
      {  //si le login deja utulisé
        $message = 'Ce pseudo est déjà utilisé, changez-le.';
      }
    }
    catch (PDOException $e)
    {//si il y a un probleme d'insertion
      $message = 'Problème dans la requête d\'insertion';
    }	
  }
  else 
  { //si un des champs pas remplie
    $message = 'Les champs Pseudo et Mot de passe doivent être remplis.';
  }
}

?>    
<section id="inscription" class="inscription-section">
        <div class="container">
            <div class="row">
				<div class="col-lg-12">
					<div class="col-lg-8 col-lg-offset-2">
						<div class="login">
							<h1>Inscription</h1>
							<form method="post">
								<input type="text" name="pseudo" placeholder="Nom de compte" required="required" /> <br>		
								<input type="password" name="pass" placeholder="Mot de passe" required="required" /><br>			
								<input type="password" name="pass_verif" placeholder="verification mot de passe" required="required" /><br>	
								<input type="text" name="nom" placeholder="nom" required="required" />		<br>	
								<input type="text" name="prenom" placeholder="prenom" required="required" /> 			<br>		
								<input type="date" name="date_naissance" placeholder="date de naissance" required="required" /><br>
								<input type="mail" name="mail" placeholder="E-mail" required="required" /><br>								
								
								<button type="submit" class="btn btn-primary btn-block btn-large">GO</button>
								<font color="red"><?php echo $message;?></font>
							</form>
						</div>
					</div>
				</div>
			</div>
        </div>
    </section>