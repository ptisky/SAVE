<?php 
include("head.html");//ajoute le head
include("../controleur/import_c.php");//récupere le fichier import

$import_b=$_POST['import_b']; //recupere ce qu'on as choisit d'export

//condition qui verifie ce qu'on as selectionné
if($import_b=="categories") { 
	Import_categories(); 
} 
if($import_b=="addresses") { 
	Import_addresses();
} 
if($import_b=="suppliers") { 
	Import_suppliers();
} 
if($import_b=="customers") { 
	Import_customers();
} 
if($import_b=="products") { 
	Import_products();
} 
if($import_b=="carts") { 
	Import_carts();
} 
if($import_b=="manufacturers") { 
	Import_manufacturers();
} 
if($import_b=="tags") { 
	Import_tags();
} 
if($import_b=="combinations") { 
	Import_combinations();
} 
if($import_b=="product_option_values") { 
	Import_product_option_values();
} 
if($import_b=="product_features") { 
	Import_product_features();
} 
if($import_b=="product_feature_values") { 
	Import_product_feature_values();
} 
if($import_b=="product_attribute") { 
	Import_product_option();
} 
if($import_b=="tout") { 
	Import_categories();	
	Import_addresses();
	Import_suppliers();
	Import_customers();
	Import_manufacturers();
	Import_tags();
	Import_product_option();
	Import_product_option_values();
	Import_product_features();
	Import_product_feature_values();
	Import_products();
	Import_combinations();
	Import_carts();
}  
include("footer.html");
$_SESSION['debug'] = false;//remet a 0 la condition de debuguage
?>