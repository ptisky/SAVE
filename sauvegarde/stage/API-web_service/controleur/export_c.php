<?php
include("connection.php");//recupere le fichier de connection
function export_API($ressource)//debut fonction
{
	try
	{
		$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG); //met la connection dans une variable	
	}
	catch (PrestaShopWebserviceException $e)
	{					
		$trace = $e->getTrace(); // On affiche les erreurs
		if ($trace[0]['args'][0] == 404) echo 'Bad ID';
		else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
		else echo 'Other error';
	}			
	try
	{
		$opt['resource'] = $ressource;  //on récupére la ressource que l'on veux
		$opt['display'] = 'full';		//full = récuperer tout les champs
		$xml = $webService->get($opt);	//afficher le resultat obtenue de prestashop grace a 'get'
		
		$opt = array('resource' => $ressource); 	//créer un tableau des résultats
		$opt['putXml'] = $xml->asXML('xml/'.$ressource.'.xml');//création d'un xml			
		$xml = $webService->edit($opt);//envoie le xml	
	}				
	catch (PrestaShopWebserviceException $ex)
	{
		$trace = $ex->getTrace();//affiche les erreurs
	}	
}//fin fonction

function export_img()
{
	try {
			$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG); //met la connection dans une variable					

			$opti['resource'] = 'images/products';//on recupere le xml de image/produit
			$opti['display'] = 'full';
			$xmli = $webService->get($opti);
		
			$opt = array('resource' => 'images/products');//met les données dans un tableau
			$opt['putXml'] = $xmli->asXML('xml/img.xml');//création d'un xml
			$xml = $webService->edit($opt);//envoie le xml
		}
		
	catch (PrestaShopWebserviceException $e)
	{
		// Here we are dealing with errors
		$trace = $e->getTrace();
		if ($trace[0]['args'][0] == 404) echo 'Bad ID';
		else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
		else echo 'Other error<br />'.$e->getMessage();
	}
					
	$img_product_xml = simplexml_load_file("xml/img.xml");	//recupe le fichier xml
									
	$cb = count ($img_product_xml->images->children());//compte le nombre de noeud dans le xml
	$loop=0;
	
	for ($i = 1; $i <= $cb; $i++) 
	{	
		$aff = PS_WS_AUTH_KEY.'@127.0.0.1/modules/prestashop2/api/images/products/'.$i.'/';//lien de l'image
										
		$get = $webService->get(array('url' => PS_SHOP_PATH.'/api/images/products/'.$i.'/'));//liens image dans la boutique
							
		foreach ($get->children()->children() as $ressources)
		{
			$loop++;
			$attributes = $ressources->attributes( 'xlink', true);
			$lien = $attributes['href'] . ' ' . $ressources . "\n";
							
			$data = $lien;
			list($part1, $part2) = explode("://", $data);//split le lien 
			$uplod = 'http://'.PS_WS_AUTH_KEY.'@'.$part2; 
							
			$img = 'img/'.$loop.'.jpg';

			file_put_contents($img, file_get_contents($uplod));//dl l'image
						
		}	
	}
}
?>